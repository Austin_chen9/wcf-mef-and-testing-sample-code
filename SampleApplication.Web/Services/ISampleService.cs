﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.Composition;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SampleApplication.Web.Services
{
	[InheritedExport]
	[ServiceContract(Name = "SampleService", Namespace = "SampleApplication.Services")]
	public interface ISampleService
	{
		[OperationContract, WebGet]
		string GetHelloWorld();


	}
}