﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ComponentModel.Composition.Hosting;

namespace SampleApplication.Web.ServiceModel
{
	public class ComposedEndpointBehavior : IEndpointBehavior
	{		
		private readonly CompositionContainer container;

		public ComposedEndpointBehavior(CompositionContainer container)
		{			
			this.container = container;
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
		{}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
		{}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
		{
			ComposedInstanceProvider provider = ComposedInstanceProvider.CreateProvider(endpoint.Address.Uri.ToString(), endpoint.Contract.ContractType, container);

			if (provider == null) throw new InvalidOperationException();

			endpointDispatcher.DispatchRuntime.InstanceProvider = provider;							
		}

		public void Validate(ServiceEndpoint endpoint)
		{}		
	}
}