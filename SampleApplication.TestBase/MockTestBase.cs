﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using System.Reflection;
using SampleApplication.Test.Base.MockExport;

namespace SampleApplication.Test.Base
{
	public abstract class MockTestBase : TestBase
	{
		protected virtual void TestSetUp(MockExportProvider mocksProvider)
		{
			base.AutoWire(mocksProvider);
		}

		protected virtual void TestSetUp(Assembly assembly , MockExportProvider mocksProvider)
		{
			AutoWire(mocksProvider, assembly);
		}

		protected TValue Any<TValue>()
		{
			return It.IsAny<TValue>();
		}

		protected String AnyString()
		{
			return Any<String>();
		}

		protected int AnyInt()
		{
			return Any<int>();
		}

		protected long AnyLong()
		{
			return Any<long>();
		}

		protected DateTime AnyDate()
		{
			return Any<DateTime>();
		}

		protected MockExportDefinition GetExport<T>(Mock<T> mock) where T : class
		{
			return new MockExportDefinition<T>(mock.Object);
		}

		protected MockExportDefinition GetExport<T>(Mock<T> mock, string contractName) where T : class
		{			
			return new MockExportDefinition<T>(contractName, mock.Object);
		}
	}
}
