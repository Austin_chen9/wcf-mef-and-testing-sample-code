﻿using System.ComponentModel.Composition.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition;

namespace SampleApplication.Test.Base.MockExport
{
	//adapted from http://pastie.org/467842

	public class MockExportProvider : ExportProvider
	{
		private List<MockExportDefinition> _exportDefinitions = new List<MockExportDefinition>();
		private Func<MockExportDefinition, object> _factory;

		public MockExportProvider(params MockExportDefinition[] definitions)
			: this(ef => { return ef.Instance; }, definitions)
		{			
		}

		public MockExportProvider(Func<MockExportDefinition, object> factory, params MockExportDefinition[] definitions)
        {
            AddExportDefinitions(definitions);
            _factory = factory;
        }

		public void AddExportDefinitions(params MockExportDefinition[] definitions)
        {
            _exportDefinitions.AddRange(definitions);
        }
		
		protected override IEnumerable<Export>  GetExportsCore(ImportDefinition definition, AtomicComposition atomicComposition)
		{
			//IEnumerable<Export> exports = new List<Export>();

			//only works for contract based definitions
			ContractBasedImportDefinition contractBasedDefinition = (ContractBasedImportDefinition)definition;

			var exports =
				_exportDefinitions.Where(importDef => importDef.ContractName == contractBasedDefinition.ContractName).
					Select(exportDef => 
							new Export(exportDef.ContractName, exportDef.Metadata, () => _factory(exportDef)));

			return exports;
		}		
	}

	
}
