﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace SampleApplication.WordConcatenator 
{
	[Export("dashConcatenator", typeof(IWordConcatenator))]
	public class DashWordConcatenator : IWordConcatenator
	{
		private const string SEPARATOR = "-";

		#region IWordConcatenator Members

		public string Concatenate(IEnumerable<string> words)
		{
			return String.Join(SEPARATOR, words);
		}

		#endregion
	}
}
