﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace SampleApplication.Services
{
	[InheritedExport]
	public interface ISampleSentenceService
	{
		string GetSentence();
	}
}
