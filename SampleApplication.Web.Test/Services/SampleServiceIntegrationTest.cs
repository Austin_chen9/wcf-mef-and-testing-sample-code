﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Test.Base;
using System.ComponentModel.Composition;
using SampleApplication.Web.Services;

namespace SampleApplication.Web.Test.Services
{
	[TestClass]
	public class SampleServiceIntegrationTest : AutoTestBase
	{
		[Import]
		private ISampleService service;

		[TestMethod]
		public void TestGetHelloWorldSuccess()
		{
			string expectedResponse = "Hello World";

			string response = service.GetHelloWorld();

			Assert.IsNotNull(response);
			Assert.AreEqual(expectedResponse, response);
		}
	}
}
