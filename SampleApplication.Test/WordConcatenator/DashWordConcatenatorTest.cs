﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition;
using SampleApplication.WordConcatenator;
using SampleApplication.Test.Base;

namespace SampleApplication.Test.WordConcatenator
{
	[TestClass]
	public class DashWordConcatenatorTest : AutoTestBase
	{
		[Import("dashConcatenator")]
		private IWordConcatenator concatenator;

		[TestMethod]
		public void TestConcatenateWithDash()
		{
			string sentence = concatenator.Concatenate(new[] { "test", "one", "two", "three" });

			Assert.IsNotNull(sentence);
			Assert.AreEqual("test-one-two-three", sentence);
		}
	}
}
