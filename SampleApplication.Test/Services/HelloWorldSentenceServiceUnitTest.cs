﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Services;
using System.ComponentModel.Composition;
using Moq;
using SampleApplication.WordProvider;
using SampleApplication.WordConcatenator;
using SampleApplication.Test.Base;
using SampleApplication.Test.Base.MockExport;

namespace SampleApplication.Test.Services
{
	[TestClass]
	public class HelloWorldSentenceServiceUnitTest : MockTestBase
	{
		[Import]
		private ISampleSentenceService service;

		private Mock<IWordProvider> wordProvider= new Mock<IWordProvider>();
		private Mock<IWordConcatenator> wordConcatenator = new Mock<IWordConcatenator>();

		public HelloWorldSentenceServiceUnitTest()
		{
			var provider = new MockExportProvider
				(
					GetExport<IWordProvider>(wordProvider),
					GetExport<IWordConcatenator>(wordConcatenator, "spaceConcatenator")
				);
			
			base.TestSetUp(provider);
		}

		[TestMethod]
		public void TestGetSentenceSuccess()
		{
			IEnumerable<string> words = new[] { "one", "two" };
			string expectedSentence = "one two";

			wordProvider.Setup(wp => wp.GetWords()).Returns(words);
			wordConcatenator.Setup(wc => wc.Concatenate(Any<IEnumerable<string>>())).Returns(expectedSentence);

			string sentence = service.GetSentence();

			Assert.IsNotNull(sentence);
			Assert.AreEqual(expectedSentence, sentence);

			wordConcatenator.Verify(wc => wc.Concatenate(words));			
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestGetSentenceFailFileNotFound()
		{
			wordProvider.Setup(wp => wp.GetWords()).Throws(new ArgumentException());

			string sentence = service.GetSentence();

		}
	}
}
