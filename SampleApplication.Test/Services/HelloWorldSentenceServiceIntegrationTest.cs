﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Services;
using System.ComponentModel.Composition;
using SampleApplication.Test.Base;

namespace SampleApplication.Test.Services
{
	[TestClass]
	public class HelloWorldSentenceServiceIntegrationTest : AutoTestBase
	{
		[Import]
		private ISampleSentenceService service;

		[TestMethod]
		public void TestGetSentenceSuccess()
		{
			const string expected = "Hello World";

			string sentence = service.GetSentence();

			Assert.IsNotNull(sentence);
			Assert.AreEqual(expected, sentence);
		}
	}
}
