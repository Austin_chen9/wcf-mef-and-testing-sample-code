﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition;
using SampleApplication.WordProvider;
using SampleApplication.Test.Base;

namespace SampleApplication.Test
{
	[TestClass]
	public class WordProviderTest : AutoTestBase
	{
		[Import]
		private IWordProvider wordProvider;

		[TestMethod]
		public void TestGetWordsSuccess()
		{
			IEnumerable<string> words = wordProvider.GetWords();

			Assert.IsNotNull(words);

			Assert.AreEqual(2, words.Count());
			Assert.AreEqual("Hello", words.First());
		}
	}
}
